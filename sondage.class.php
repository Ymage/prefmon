<?php
/* 
  Copyright 2016 SamuelBF
  Copyright 2016 Alexis Bienvenüe

  Ce fichier fait partie du logiciel Prefmon, logiciel libre placé sous la 
  license GNU General Public License version 3. Vous devriez avoir reçu une
  copie de la license avec ce logiciel. Si ça n'est pas le cas, vous pouvez 
  la trouver en ligne à l'adresse : <http://www.gnu.org/licenses/>.
*/

/* Classe sondage : le sondage est réalisé lors de la construction de l'objet.
   Les résultats sont disponibles ensuite dans la variable publique 'résultat', 
   mais en pratique on utilise la fonction versSQL qui produit une chaîne de la
   forme : (IDProcédure, Date, heureDébut, heureFin, date1, date2) 

   En pratique, cette classe ne fait pas l'examen du site distant. Ce sont ses enfants 
   qui, lors de leur construction vont réaliser cet examen  en se servant des fonctions
   que propose la classe sondage.

*/

class Sondage {

  protected $basefichier;
  protected $IDProcédure;
  protected $connexion;
  protected $heureDébut;
  protected $heureFin;
  protected $date;
  public $résultat;
  private $log, $cookies;
  
  function __construct($log, $dossier, $paramètres) {
    $this->log = $log;
    $this->résultat = [];
    $this->date = date('Y-m-d');
    $this->heureDébut = date('H:i:s');
    $this->cookies = isset($paramètres['cookies']) ? $paramètres['cookies'] : FALSE;

    # On vérifie qu'on a bien l'ID du sondage
    if(isset($paramètres['IDProcédure'])) {
      $this->IDProcédure = $paramètres['IDProcédure'];
    } else {
      $this->IDProcédure = 0;
      $this->log(Log::AVERTISSEMENT, 'Aucun ID n\'a été précisé pour ce sondage.');
    }

    $this->basefichier = $dossier.'/'.strtr($this->date, '-', '/').'/'.
      strtr($this->heureDébut, ':', '-');
    if(!is_dir(dirname($this->basefichier))) {
      if(!mkdir(dirname($this->basefichier), 0755, true)) {
        $this->log(Log::ERREUR, "Impossible de créer le dossier " . dirname($this->basefichier));
        return FALSE;
      }
    }
    
    $this->log(Log::DEBUG, "Ouverture de la session cURL");
    if(!($this->connexion = curl_init())) {
      $this->log(Log::ERREUR, "Échec d'ouverture de la session cURL");
      return FALSE;
    }

    curl_setopt($this->connexion, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($this->connexion, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($this->connexion, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($this->connexion, CURLOPT_ENCODING, "");
    curl_setopt($this->connexion, CURLOPT_TIMEOUT, 6);
    # Ajout de cookies si besoin :
    if($this->cookies) {
      curl_setopt($this->connexion, CURLOPT_COOKIEJAR, $this->basefichier.'.cookies');
      curl_setopt($this->connexion, CURLOPT_COOKIEFILE, $this->basefichier.'.cookies');
    }

    if(isset($paramètres["useragent"]) && $paramètres["useragent"]) {
       curl_setopt($this->connexion, CURLOPT_USERAGENT, $paramètres["useragent"].' [alea passe-filtre :'.rand().']') or
        $this->log(Log::AVERTISSEMENT, "Échec de fixation du User-Agent pour cURL.");
    }
    # Utilisation éventuelle d'un proxy
    if(isset($paramètres["proxy"]) && $paramètres["proxy"]) {
        $this->log(Log::DEBUG, "Proxy : ".$paramètres["proxy"]);
        curl_setopt($this->connexion, CURLOPT_PROXY, $paramètres["proxy"]);
    }
    
    return TRUE;
  }

  # Renvoie 'NULL' si la date est nulle, la date entre guillemets si elle est non nulle.
  protected function dateSQL($date) {
    return is_null($date) ?  "NULL": '"'.$date.'"';
  }
  
  # La fonction fermeture_connexion ferme la connexion et enregistre l'heure de fin du processus
  # de téléchargements.
  protected function fermeture_connexion() {
    $this->log(Log::DEBUG, "Fermeture de la session cURL");
    curl_close($this->connexion);
    $this->heureFin = date('H:i:s');
  }

  # Si on a rencontré une erreur quelque part, on vide les résultats
  # partiels puis on ferme la connexion
  protected function abandon() {
    $this->résultat=[];
    $this->fermeture_connexion();
  }

  # Le planning est vide !
  protected function planning_vide() {
    $this->résultat=[null, null];
    $this->fermeture_connexion();
  }

  public function versSQL() {
    if(count($this->résultat)) {
      return ['(0x'.$this->IDProcédure.', "'.$this->date.'", "'.$this->heureDébut.'", "'.$this->heureFin.'", '.$this->dateSQL($this->résultat[0]).', '.$this->dateSQL($this->résultat[1]).')'];
    } else {
      return [];
    }
  }
  
  # fonction charger_page : télécharge une URL vers un fichier local et renvoie l'objet
  # correspondant. Si le fichier (avec cet identifiant) existe, il est renvoyé sans être à
  # nouveau téléchargé.
  # Gère les erreurs dans $this->log, renvoie FALSE si échec.
  # Format : HTML (par défaut, renvoie un objet DOMDocument), JSON (renvoie le json_decode du
  # contenu du fichier) ou raw (renvoie le contenu brut du fichier)
  protected function charger_page($url, $identifiant, $format = 'html') {
    $nom_fichier = $this->basefichier.'_'.$identifiant.'.'.$format;
    # Si le fichier n'existe pas encore ou si c'est une requête POST, on télécharge la page :
    if(!file_exists($nom_fichier)) {
      $this->log(Log::DEBUG, "Ouverture du fichier $nom_fichier");
      if(!($fichier = fopen($nom_fichier, 'w'))) {
        $this->log(Log::ERREUR, "Échec d'ouverture du fichier $nom_fichier");
        return FALSE;
      }
      $this->log(Log::INFORMATION, "Téléchargement de $url");
      curl_setopt($this->connexion, CURLOPT_URL, $url);
      curl_setopt($this->connexion, CURLOPT_FILE, $fichier);
      if(!curl_exec($this->connexion)) {
        $this->log(Log::ERREUR, "Échec de téléchargement de $url (" . curl_errno($this->connexion) . ") : " . curl_error($this->connexion));
        return FALSE;
      }
      $httpCode = curl_getinfo($this->connexion, CURLINFO_HTTP_CODE);
      if($httpCode >= 400) {
        $this->log(Log::ERREUR, "Problème lors du téléchargement de $url (code de retour : " . $httpCode . ").");
        return FALSE;
      }
      $this->log(Log::DEBUG, "Fermeture du fichier $nom_fichier");
      fclose($fichier);
    }

    switch($format) {
    case 'json':
      $résultat = json_decode(file_get_contents($nom_fichier));
      if(!$résultat) {
        $this->log(Log::ERREUR, "Échec de lecture (JSON) de $nom_fichier. On essaie de continuer quand même.");
        return FALSE;
      }
      break;
    case 'raw':
      $résultat = file_get_contents($nom_fichier);
      break;
    default:
      $résultat = new DOMDocument();
      $this->log(Log::DEBUG, "Chargement (arbre HTML) du fichier $nom_fichier");
      if(!@$résultat->loadHTMLFile($nom_fichier)) {
        $this->log(Log::ERREUR, "Échec de chargement (arbre HTML) de $nom_fichier. On essaie de continuer quand même.");
        return FALSE;
      }
      break;
    }
    return $résultat;
  }


  # Recherche d'IDs : 
  # Cas simple : l'ID est donné en paramètre (XXXid)
  # Cas simple 2 : on a pas de paramètre XXXid/id-/preg/preg- :
  #  - on charge la page sur laquelle on trouve ces IDs
  #  - on cherche les items correspondant à $XPath
  #  - on extrait pour chaque item la valeur.
  # Cas complexe : il est donné négativement ("tout sauf XXX") ou sous forme d'expression régulière :
  #  - on charge la page sur laquelle on trouve ces IDs
  #  - on cherche les items correspondant à $XPath
  #  - on extrait pour chaque item la valeur et l'attribut.
  #  - on filtre par valeur pour "tout sauf XXX" (XXXid-), par attribut pour les expressions régulières.
  # Effet de bord : si on ne trouve rien, on abandonne avec un message d'erreur.
  protected function chercher_ids($parametre, $url, $cle_url, $XPath, $extraire_attribut, $extraire_valeur) {
    # Si le(s) ID(s) sont indiqué(s) dans un paramètre nommé, on le renvoie sous forme de tableau :
    if(isset($this->paramètres[$parametre.'id']))
      return explode(',', $this->paramètres[$parametre.'id']);
      
    # Sinon, on cherche dans la page voulue.
    # Plusieurs modes de recherche :
    if(isset($this->paramètres[$parametre.'id-'])) { # Cas où l'ID est en mode "tout sauf X,Y,Z
      $tableau_exclus = explode(',', $this->paramètres[$parametre.'id-']);
      $filtre = function($attribut) use ($tableau_exclus) { return !in_array($x, $tableau_exclus); };
    } else if(isset($this->paramètres[$parametre.'preg'])) { # Cas où on cherche l'expression '/xxx/'
      $filtre_preg = $this->paramètres[$parametre.'preg'];
      $filtre = function($attribut) use ($filtre_preg) { return preg_match($filtre_preg, $attribut); };
    } else if(isset($this->paramètres[$parametre.'preg-'])) { # Cas où on cherche tout sauf l'expression '/xxx/'
      $filtre_preg = $this->paramètres[$parametre.'preg-'];
      $filtre = function($attribut) use ($filtre_preg) { return !preg_match($filtre_preg, $attribut); };
    } else {
      $this->log(Log::INFORMATION, 'Pas de paramètre nommé '.$parametre.'id/id-/preg ou preg- pour ce sondage. On prend tous les items');
      $filtre = function($x) { return true; };
    }

    # On charge la page :
    $html = $this->charger_page($url,$cle_url);
    if(!$html) {
      $this->log(Log::ERREUR, 'Impossible de charger la page '.$url.' nécessaire à l\'identification des ID '.$parametre.'.');
      $this->abandon();
      return array();
    }
    $liste = new DOMXpath($html);
    $items = $liste->query($XPath);
    $this->log(Log::DEBUG, 'On a trouvé '.$items->length.' items correspondant à l\'expression XPath sur la page '.$url.'.');
    $resultat = array();
    foreach($items as $item) {
      if($filtre($extraire_attribut($item))) {
        $resultat[] = $extraire_valeur($item);
        $this->log(Log::DEBUG, '(OK) '.$parametre.'id='.$extraire_valeur($item));
      } else {
        $this->log(Log::DEBUG, '(..) '.$parametre.'id='.$extraire_valeur($item));
      }
    }
    
    if(!count($resultat)) {
      $this->log(Log::ERREUR, 'Aucun '.$parametre.'id trouvé. Vérifiez les paramètres.');
      $this->abandon();
      return FALSE;
    }

    return $resultat;
  }

  # insérer_résultat : [date1, date2], date3 --> tableau des 2 plus petits parmi les 3 
  protected function insérer_résultat($résultat, $date) {
    if(is_null($date)) return $résultat;
    if(is_null($résultat[0]) or $date < $résultat[0]) {
      $résultat[1] = $résultat[0];
      $résultat[0] = $date;
    } else if ($date > $résultat[0] and (is_null($résultat[1]) or $date < $résultat[1])) {
      $résultat[1] = $date;
    }
    return $résultat;
  }

  # Log : enregistrement d'un message préfixé de l'ID de la procédure en cause
  protected function log($niveau, $str) {
    $this->log->message($niveau, '[#'.$this->IDProcédure.'] '.$str);
  }

}

?>
