CREATE TABLE Procédures(
       ID INT UNSIGNED PRIMARY KEY NOT NULL,
       Département SMALLINT UNSIGNED NOT NULL,
       NomDossier VARCHAR(200),
       NomProcédure VARCHAR(60),
       Exclusif BOOLEAN DEFAULT FALSE,
       Description TEXT,
       DateDébut Date,
       Périodes VARCHAR(80));


CREATE TABLE Sondages(
       ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
       IDProcédure INT UNSIGNED NOT NULL,
       DateSondage DATE NOT NULL,
       HeureDébut TIME NOT NULL,
       HeureFin TIME NOT NULL,
       Date1 DATE,
       Date2 DATE,
       INDEX (IDProcédure,DateSondage));


DELIMITER //


DROP FUNCTION IF EXISTS Couleur //
CREATE FUNCTION Couleur(DateSondage DATE, Date1 DATE, Date2 DATE)
RETURNS ENUM('vert','jaune','orange','rouge','noir')
IF ISNULL(Date1) AND ISNULL(Date2) THEN
   RETURN "noir";
ELSEIF DATEDIFF(Date1, DateSondage) > 60 THEN
   RETURN "rouge";
ELSEIF DATEDIFF(Date1, DateSondage) > 30 THEN
   RETURN "orange";
ELSEIF ISNULL(Date2) OR DATEDIFF(Date2, DateSondage) >= 30 THEN
   RETURN "jaune";
ELSE
   RETURN "vert";
END IF //

DELIMITER ;
