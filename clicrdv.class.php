<?php
/* 
  Copyright 2016 SamuelBF
  Copyright 2016 Alexis Bienvenüe

  Ce fichier fait partie du logiciel Prefmon, logiciel libre placé sous la 
  license GNU General Public License version 3. Vous devriez avoir reçu une
  copie de la license avec ce logiciel. Si ça n'est pas le cas, vous pouvez 
  la trouver en ligne à l'adresse : <http://www.gnu.org/licenses/>.
*/

/* clicrdv : moteur de prise de RDV utilisé par la préfecture du Rhône
   Repose sur le site clicrdv.com, qui a une API dédiée (communication en JSON dans les 2 sens).

   Pour certaines démarches, il est nécessaire de s'identifier. Un couple login/mot de passe 
   correspondant à une adresse mail jetable a été créé et est utilisé ici par défaut.
*/

include_once 'configuration.php';
include_once 'log.class.php';
include_once 'sondage.class.php';

class Clicrdv extends Sondage {

  protected $intervid, $groupid, $calendarid, $loginnecessaire, $loginuser, $loginpass;
  
  function __construct($log, $dossier, $paramètres) {

    # Si on a besoin de s'enregistrer, on aura besoin de cookies :
    if(isset($paramètres['loginnecessaire']))
      $paramètres['cookies'] = TRUE;

    if(!parent::__construct($log, $dossier, $paramètres)) {
      return;
    }

    $this->intervid = $paramètres['intervid'];
    $this->groupid = $paramètres['groupid'];
    $this->calendarid = isset($paramètres['calendarid']) ? $paramètres['calendarid'] : '';
    $this->userid = FALSE;

    // Paramètres par défaut : pas besoin de login / utilisateur par défaut / hash mdp par défaut
    // Le hash du mot de passe a été trouvé en sniffant une connexion avec des paramètres qui
    // fonctionnaient (le mot de passe en clair est 'prefmon').
    $this->loginnecessaire = isset($paramètres['loginnecessaire']) ? $paramètres['loginnecessaire'] : FALSE;
    $this->loginuser = isset($paramètres['loginuser']) ? $paramètres['loginuser'] : '2h9aik+5h3ixms3c0is@guerrillamail.net';
    $this->loginpass = isset($paramètres['loginpass']) ? $paramètres['loginpass'] : 'f34792f78a0c503a694239a2bfece845a5d235dd';

    curl_setopt($this->connexion, CURLOPT_REFERER, 'https://www.clicrdv.com/?intervention_ids[]='.$this->intervid);

    # Connexion au site web (si nécessaire)
    if($this->loginnecessaire) {
      if(!$this->login()) {
        $this->log(Log::ERREUR, 'Échec d\'authentification (identifiants : '.$this->loginuser.' : '.$this->loginpass.')');
        return;
      }
    }
    
    $apiparams = 'intervention_ids[]='.$this->intervid.'&group_id='.$this->groupid;
    $url_base = 'https://www.clicrdv.com/api/v1/availabletimeslots?'.$apiparams.'&format=json&nDays=7&calendar_id='.$this->calendarid;;

    // Ajout de l'UserID si nécessaire :
    if($this->userid) {
      $url_base .= '&api_profile=Account&api_user_id='.$this->userid;
    } else {
      $url_base .= '&api_profile=NotloggedUser';
    }
    
    $résultat = [null, null];
    $nbpages = 0; // S'assurer qu'on ne boucle pas indéfiniment
    $url = $url_base;
    do {
      if(!$json = $this->charger_page($url, 'a_'.$nbpages, 'json')) {
        # Si on y arrive pas, on s'arrête :
        $this->fermeture_connexion();
        return;
      }
      
        # Recherche des plages libres :
      if(isset($json->availabletimeslots)) {
        foreach($json->availabletimeslots as $dateHeure) {
        $date = substr($dateHeure->start, 0, 11);
        $this->log(Log::INFORMATION, "Date trouvée : $date");
        $résultat = $this->insérer_résultat($résultat, $date);     
        } 
      }

      # calcul de l'URL suivante :
      $datesuiv=(new DateTime($json->firstSlot))->add(new DateInterval('P1W'));
      $url = $url_base . '&start='.urlencode($datesuiv->format('Y-m-d 00:00:00'));
    } while(isset($contenu[0]) and (is_null($résultat[0]) or is_null($résultat[1])) and $nbpages < 14);
    
    # On enregistre le résultat
    $this->résultat = $résultat;
    $this->fermeture_connexion();
  }

  # fonction Login : s'enregistrer si besoin
  private function login() {

    $this->log(Log::DEBUG, 'Authentification sur apirdv.com avec comme identifiants '.$this->loginuser.' : '.$this->loginpass);
    $données = '{"group_id":'.$this->groupid.',"account":{"email":"'.$this->loginuser.'","password":"'.$this->loginpass.'"},"format":"json","api_profile":"NotloggedUser"}';
    curl_setopt($this->connexion, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($this->connexion, CURLOPT_POSTFIELDS, $données);
    curl_setopt($this->connexion, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($données)));

    if(!$résultat = $this->charger_page('http://www.clicrdv.com/api/v1/sessions/login', 'login', 'json'))
      return FALSE;

    // extraction de l'ID Utilisateur
    if(!isset($résultat->account->id)) {
      $this->log(Log::ERREUR, 'Authentification : on a pas pu récupérer l\'ID utilisateur dans le fichier _login.');      
      return FALSE;
    }
    $this->userid = $résultat->account->id;

    // Remise à 0 des options curl :
    curl_setopt($this->connexion, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($this->connexion, CURLOPT_POSTFIELDS, NULL);
    curl_setopt($this->connexion, CURLOPT_HTTPHEADER, []);

    $codeRetour = curl_getinfo($this->connexion, CURLINFO_HTTP_CODE);    
    # On renvoie FALSE (échec) si on obtient le code 401 (échec de connexion)
    if($codeRetour==401)
      return FALSE;

    # Si le code est différent de 200 (tout va bien), on affiche un avertissement :
    if($codeRetour!=200)
      $this->log(Log::AVERTISSEMENT, 'Authentification : on a obtenu '.$codeRetour.' comme réponse à la demande (voir le fichier _login.json pour plus de détails)');


    $this->log(Log::DEBUG, 'Authentification réussie (a priori)');
    return TRUE;
  }
}

?>