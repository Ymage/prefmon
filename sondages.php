<?php
/* 
  Copyright 2016 SamuelBF

  Ce fichier fait partie du logiciel Prefmon, logiciel libre placé sous la 
  license GNU General Public License version 3. Vous devriez avoir reçu une
  copie de la license avec ce logiciel. Si ça n'est pas le cas, vous pouvez 
  la trouver en ligne à l'adresse : <http://www.gnu.org/licenses/>.
*/

/* sondages.php : 
   appelle toute les classes de type sondage et interprète un ensemble de paramètres pour 
   exécuter le sondage correspondant.
 */

include_once 'configuration.php';
include_once 'log.class.php';
include_once 'sondage.class.php';
include_once 'ezbooking_41.class.php';
include_once 'clicrdv.class.php';
include_once 'eappointment.class.php';

# Fonction exécuter_sondage : charger d'interpréter le paramètre système et d'appeler les
# classes correspondantes.
function exécuter_sondage($log, $dossier, $paramètres) {
  $sondage=null;
  if(!isset($paramètres['système'])) {
    $log->message(Log::ERREUR, 'Pas de système enregistré pour la procédure #'.$paramètres['IDProcédure'].'. Aucune action effectuée.');
  } else {
    switch($paramètres['système']) {
    case 'EZBooking_41':
      $sondage=new EZBooking_41($log, $dossier, $paramètres);
      break;
    case 'Clicrdv':
      $sondage=new Clicrdv($log, $dossier, $paramètres);
      break;
    case 'Eappointment':
      $sondage=new eappointment($log, $dossier, $paramètres);
      break;
    default:
      $log->message(Log::ERREUR, 'Système '.$paramètres['système'].' inconnu pour la procédure #'.$paramètres['IDProcédure'].'. Aucune action effectuée.');
      break;
    }
  }
  if(count($sondage->résultat)) {
    $log->message(Log::INFORMATION,'Résultats du sondage : '.$sondage->résultat[0].', '.$sondage->résultat[1]);
  } else {
    $log->message(Log::INFORMATION,'Erreur lors du sondage');
  }
  return($sondage);
}

?>
