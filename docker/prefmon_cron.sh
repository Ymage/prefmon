#!/usr/bin/env bash
set -euo pipefail

php /var/app/effectuer_sondage.php ${SONDAGE_PARAMS}
