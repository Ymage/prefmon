<?php
/* 
  Copyright 2016 SamuelBF
  Copyright 2016 Alexis Bienvenüe

  Ce fichier fait partie du logiciel Prefmon, logiciel libre placé sous la 
  license GNU General Public License version 3. Vous devriez avoir reçu une
  copie de la license avec ce logiciel. Si ça n'est pas le cas, vous pouvez 
  la trouver en ligne à l'adresse : <http://www.gnu.org/licenses/>.
*/

/* EZBooking_41 : premier moteur d'examen des RDV disponibles. 
   c'est le système déployé dans la plupart des préfectures examinées
   jusqu'à présent.

   En pratique, il y a plusieurs pages :
    - La première où l'on confirme respecter les conditions d'usage
    - Une éventuelle seconde où on peut choisir entre plusieurs types de RDV
      (parfois, il s'agit du choix entre plusieurs démarches possibles, parfois
      il s'agit du choix entre plusieurs plannings pour une même démarche)
    - S'il y a lieu, une troisième qui indique qu'aucun RDV n'est disponible. 
      si des RDV sont disponibles, on est redirigé automatiquement vers la 4e : 
    - La quatrième contient un planning des RDV disponible, chargé dynamiquement.
      En statique, on y récupère donc simplement cette URL vers le planning effectif, 
      on télécharge ce planning et on y lit les RDV proposés.

*/

include_once 'configuration.php';
include_once 'log.class.php';
include_once 'sondage.class.php';

class EZBooking_41 extends Sondage {

  protected $paramètres;

  protected function lire_page($baseid, $planningid, $page) {
    
    $url = $this->paramètres['baseurl'].'/booking/create/'.$baseid.'/'.$page;
    $fichier = $baseid.'.'.$planningid.'.'.$page;
    $html = $this->charger_page($url,$fichier);

    if(!$html) {
      $this->log(Log::ERREUR, "Échec de lecture de $url.");
      return(false);
    }
    $contenu = new DOMXpath($html);

    # S'il s'agit d'une redirection, on passe tout de suite à la page suivante:
    if($nouvellepage = $this->lire_redirection())
      return($this->lire_page($baseid, $planningid, $nouvellepage));
    
    
    $form = $contenu->query('//form[@id="FormBookingCreate"]//input[@type="submit"]/@value');
    $header = $contenu->query('//a[@title="Redirection vers page d\'accueil"]');

    # On regarde d'abord s'il y a une indication comme quoi il n'y a pas de rendez-vous dispo :
    if(isset($form[0]) and ($form[0]->value == 'Terminer' or $form[0]->value == 'Finish')) {
      $this->log(Log::DEBUG, "La page indique qu'il n'y a pas de RDV disponible");
      return true;
    }
    
    if(isset($header[0]) and preg_match("/Cette page n'est pas disponible/",$header[0]->nodeValue)) {
      $this->log(Log::DEBUG, "La page n'est pas disponible");
      return true;
    }
    
    # Puis on vérifie qu'on peut bien aller plus loin
    if(!isset($form[0])) {
      $this->log(Log::ERREUR, 'Pas de bouton "submit" sur la page '.$url.' / '.$fichier.'. Abandon.');
      return false;
    }
    if(!in_array($form[0]->value, ['Etape suivante', 'Next step', 'PremiÃ¨re plage horaire libre', 'First free timeslot', 'Effectuer une demande de rendez-vous', 'Create a new booking'])) {
      $this->log(Log::ERREUR, 'Valeur du bouton "submit" non reconnue sur la page '.$url.' / '.$fichier.' (valeur trouvée: "' . $form[0]->value . '"). Abandon.');
      return false;
    }

    # Chargement du formulaire sur la page présente :
    switch($page) {
      case 0: # On accepte les conditions d'utilisation
        if(($nouvellepage = $this->valider_formulaire($url, $fichier, $page, ['condition'=>'on','nextButton'=>$form[0]->value])) === FALSE)
          return false;
        
        # Si on reste sur la ême page, c'est qu'on a rencontré un message d'erreur
        if($nouvellepage == 0)
          return true;
        
        return($this->lire_page($baseid, $planningid, $nouvellepage));
        break;
        
      case 1: # Choix d'un planningid
        $plannings = $this->chercher_ids('planning', $url, $fichier, '//label[starts-with(@for,"planning")]',
          function($x) {return $x->textContent;}, function($x) { return preg_replace('/^planning(\d+)$/','${1}', $x->attributes->getNamedItem("for")->value); });
        foreach($plannings as $planning) {
          if(!($nouvellepage = $this->valider_formulaire($url, $baseid.'.'.$planning.'.'.$page, $page, ['planning'=>$planning,'nextButton'=>$form[0]->value])))
            return false;
          
          $this->log(Log::INFORMATION,"Consultation de la sous-tâche ".$planning);
          if(!$this->lire_page($baseid, $planning, $nouvellepage)) {
            return(false);
          }
        }
        return(true);
        break;
        
      case 2:
      case 3: # Cliquer "Etape suivante"
        if(!($nouvellepage = $this->valider_formulaire($url, $fichier, $page, ['nextButton'=>$form[0]->value])))
          return false;
          
        return($this->lire_page($baseid, $planningid, $nouvellepage));
        break;
      
      case 4: # Page 'planning'
        $planningurl = $contenu->query('//a[@id="ezplanning-url"]/@href');
        if(!isset($planningurl[0]))
          return(false);

        return($this->ajouter_plages($baseid.'.'.$planningid.'.5', $planningurl[0]->value));
        break;
        
      default:
        $this->log(Log::ERREUR, 'On est sur la page '.$page.', ce qui n\'est pas sensé arriver. Abandon.');
        return(false);
        
    }
    
  }

  # lire_redirection: renvoie false si la dernière requête n'est pas une redirection, ce qui suit le dernier '/'
  # dans l'URL vers laquelle on est redirigé s'il y a redirection.
  protected function lire_redirection() {
    # La redirection se fait vers une page dont l'URL est xxxxx/baseid/n où n est le numéro suivant de page:
    $url_redirection = curl_getinfo($this->connexion, CURLINFO_REDIRECT_URL);
    if(empty($url_redirection)) {
      return(false);
    } else {
      return(substr($url_redirection, strrpos($url_redirection, '/')+1));
    }
  }
  
  protected function valider_formulaire($url, $pr_fichier, $page, $postfields) {
    curl_setopt($this->connexion, CURLOPT_POST, TRUE);
    curl_setopt($this->connexion, CURLOPT_POSTFIELDS, $postfields);
    $html = $this->charger_page($url,$pr_fichier.'.redirection');
    curl_setopt($this->connexion, CURLOPT_POSTFIELDS, []);
    curl_setopt($this->connexion, CURLOPT_POST, false);
         
    # La redirection se fait vers une page dont l'URL est xxxxx/baseid/n où n est le numéro suivant de page:
    $nouvellepage = $this->lire_redirection();
    $this->log(Log::DEBUG, 'La page '.$page.' envoie vers la page '.$nouvellepage.'.');
    
    # Si on reste sur la même page, c'est a priori qu'il y a un message d'erreur
    if(!$nouvellepage) {
      if($html) {
        $contenu = new DOMXpath($html);
        $error = $contenu->query('//div[@id="inner_Booking"]//ul[@class="error"]/li');
        if(isset($error[0]) and preg_match("/Il n'y a pas calendrier disponible/",$error[0]->nodeValue)) {
          $this->log(Log::AVERTISSEMENT, "La page indique qu'il n'y a pas de calendrier disponible");
          return $page;
        } elseif(isset($error[0])) {
          $this->log(Log::ERREUR, 'Erreur renvoyée par le serveur : '.$error[0]->nodeValue);
        }
      }
    }
    if($nouvellepage <= $page or !$nouvellepage) {
      $this->log(Log::ERREUR, 'La validation du formulaire page '.$page.' a échoué. Abandon.');
      return false;
    }
    
    return $nouvellepage;
  }
  
  protected function ajouter_plages($pr_fichier,$planning_url) {
    $résultat = array(null, null);
    $pagesvides = 0; // S'assurer qu'on ne boucle pas indéfiniment
    do {
      $url = $this->paramètres["baseurl"].$planning_url;
      # L'URL est de la forme http://...../...::XXXX::numpage -> on récupère numpage pour nommer
      # nos fichiers en local :
      if(!$html = $this->charger_page($url, $pr_fichier.'.'.substr($planning_url, strrpos($planning_url, ':')+1))) {
        # Si on y arrive pas, on s'arrête :
          return(false);
      }

      # Recherche des plages libres :
      $plageslibres = (new DOMXpath($html))->query('//td[contains(@class,"free")]/a/@href');
      # Ce lien est de la forme http://<plein de choses>/XXXXXXXX où la dernière valeur est le
      # timestamp de la date libre.
      foreach($plageslibres as $url) {
        $ts = (int) substr($url->value, strrpos($url->value, '/')+1);
        $date = date('Y-m-d', $ts);
        $this->log(Log::INFORMATION, "Date trouvée : $date");
        $résultat = $this->insérer_résultat($résultat, $date);
      }
      
      $this->log(Log::DEBUG, "$plageslibres->length plage(s) trouvée(s)");

      if(!$plageslibres->length) { $pagesvides++;
      } else { $pagesvides = 0; }

      # Recherche de la page suivante : 
      $contenu = (new DOMXpath($html))->query('//th[@class="nav"]/a[contains(@class,"next")]/@href');
      $planning_url = isset($contenu[0]) ? $contenu[0]->value : '';

    } while(!empty($planning_url) and (is_null($résultat[0]) or is_null($résultat[1])) and $pagesvides < 14);

    $this->résultat = $this->insérer_résultat($this->résultat, $résultat[0]);
    $this->résultat = $this->insérer_résultat($this->résultat, $résultat[1]);
    return(true);
  }
  
  function __construct($log, $dossier, $paramètres) {
    $paramètres['cookies'] = isset($paramètres['cookies']) ? $paramètres['cookies'] : TRUE;
    if(!parent::__construct($log, $dossier, $paramètres)) {
      return;
    }
    $this->résultat = [null,null];

    $this->paramètres = $paramètres;
    
    # La page http://dpt.gouv.fr/rendez-vous est une redirection vers la page listant les rendez-vous.
    curl_setopt($this->connexion, CURLOPT_FOLLOWLOCATION, TRUE);
    $baseids = $this->chercher_ids('base', 
                isset($this->paramètres['baseidurl']) ? $this->paramètres['baseidurl'] : $this->paramètres['baseurl'].'/rendez-vous', 
                'L', '//a[contains(@href,"create/")]',
                function($x) { return $x->textContent; },
                function($x) { return preg_replace('#^.*/booking/create/(\d+)$#','${1}', $x->attributes->getNamedItem("href")->value); });
    if(!$baseids) {
      return;
    }
    curl_setopt($this->connexion, CURLOPT_FOLLOWLOCATION, FALSE);

    foreach($baseids as $baseid) {
      if(!$this->lire_page($baseid,0,0)) {
        $this->abandon();
        return;
      }
    }
  
    # Fin !
    $this->fermeture_connexion();
  }
}

?>
